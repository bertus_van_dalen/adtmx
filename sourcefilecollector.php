<?php

class SourceFileCollector{
	
	private $_logger;
	private $_targetDir;
	private $_sourceDir;
	
	function __construct($logger, $targetDir, $sourceDir){
		$this->_logger = $logger;
		$this->_targetDir = $targetDir;
		$this->_sourceDir = $sourceDir;
	}
	
	function collect($mp3s){
		$this->_logger->log("loading " . json_encode($mp3s));
		foreach($mp3s as $mp3){
			$mp3file = $mp3 . ".MP3";
			$wavFile = $mp3 . ".WAV";
			$targetfile = $this->_targetDir . $mp3file;
			if(!is_file($targetfile))
			{
				$dlFrom = $this->_sourceDir . $mp3file;
				$this->_logger->log("downloading " . $dlFrom);
				$fileContents = file_get_contents($dlFrom);
				if($fileContents === false){
					$this->_logger->log("ERROR - the download did not succeed");
					die("download mp3 fail");
				}else{
					$this->_logger->log("writing downloaded content to " . $targetfile);
					file_put_contents($targetfile, $fileContents);
				}
			}
			if(!is_file($this->_targetDir . $wavFile)){
				set_time_limit(30);
				$c = 'mpg123 -w ' . $this->_targetDir . $wavFile . ' ' . $this->_targetDir . $mp3file . ' 2>&1';
				$this->_logger->log(shell_exec($c)); 
			}
		}

	}
}