<?php

class FileManager {
    
	const LOCKFILE_NAME = 'lock.txt';
	
	private $_location;
	private $_logger;
	private $_file;
	private $_receipt_file;
	private $_receipt_postfix = "_receipt";
	private $_json_file;
	private $_jsontask_postfix = "_json";
	private $_ecasound_file;
	private $_ecasound_command_postfix = "_ecasound";
	
	private $_input_message_file_loc;
	private $_input_message_pop_receipt_file_loc;
	private $_input_message_json_file_loc;
	private $_ecasound_command_file_loc;
	
	function __construct($location, $output_location, $logger) {
		$this->_location = $location;
		$this->_output_location = $output_location;
		$this->_logger = $logger;
    }
	
	function seeWhichFilesArePresent(){
		$handle = opendir($this->_location);
		if(!$handle){
			$this->_logger->log("ERROR - could not open " . $this->_location);
			exit(1);
		}
		while(false !== ($entry = readdir($handle))){
			if($entry != "." && $entry != ".."){
				
				if(substr($entry, strlen($entry) - strlen($this->_receipt_postfix)) === $this->_receipt_postfix){
					if($this->_receipt_file){
						$this->_logger->log("ERROR - there seems multiple receipt files in the lock");
						exit(1);
					}
					$this->_receipt_file = $entry;
				}
				else if(substr($entry, strlen($entry) - strlen($this->_jsontask_postfix)) === $this->_jsontask_postfix){
					if($this->_json_file){
						$this->_logger->log("ERROR - there seems multiple json files in the lock");
						exit(1);
					}
					$this->_json_file = $entry;
				}
				else if(substr($entry, strlen($entry) - strlen($this->_ecasound_command_postfix)) === $this->_ecasound_command_postfix){
					if($this->_ecasound_file){
						$this->_logger->log("ERROR - there seems multiple json files in the lock");
						exit(1);
					}
					$this->_ecasound_file = $entry;
				}
				else{
					if($this->_file){
						$this->_logger->log("ERROR - there seems multiple message files in the lock");
						exit(1);
					}
					$this->_file = $entry;
				}		
			}
		}
		closedir($handle);
	}
	
	function looksBusy($expirySeconds){
		if($this->_file){
			$f = $this->_location . $this->_file;
			$tooOld = time() - $expirySeconds;
			$ctime = filectime($f);
			if($ctime <= $tooOld){
				$this->flush();
			}
			else
			{
				return true;
			}
		}
		return false;
	}
	
	function isLocked(){
		return $this->_file === self::LOCKFILE_NAME;
	}
	
	function lock(){
		$this->_file = self::LOCKFILE_NAME;
		$this->_input_message_file_loc = $this->_location . $this->_file;
		if(false === file_put_contents($this->_input_message_file_loc, $message)){
			$this->_logger->log("ERROR - could not write to " . $this->_input_message_file_loc);
			exit(1);
		}
	}
	
	function unlock(){
		if(!$this->isLocked()){
			$this->_logger->log("ERROR - you are unlocking but there is no lock file. You cannot unlock when there no lock file. Maybe the system is busy and you have to wait for it to finish??");
			exit(1);
		}
		$this->_input_message_file_loc = $this->_location . $this->_file;
		unlink($this->_input_message_file_loc);
	}
	
	function writeMessageFile($id, $message){
		$this->_input_message_file_loc = $this->_location . $id;
		$this->_logger->log("writing " . $this->_input_message_file_loc);
		if(false === file_put_contents($this->_input_message_file_loc, $message)){
			$this->_logger->log("ERROR - could not write to " . $this->_input_message_file_loc);
			exit(1);
		}
	}
	
	function writePopReceiptFile($id, $receipt){
		$this->_receipt_file = $id . $this->_receipt_postfix;
		$this->_input_message_pop_receipt_file_loc = $this->_location . $this->_receipt_file;
		$this->_logger->log("writing " . $this->_input_message_pop_receipt_file_loc);
		if(false === file_put_contents($this->_input_message_pop_receipt_file_loc, $receipt)){
			$this->_logger->log("ERROR - could not write to " . $this->_input_message_pop_receipt_file_loc);
			exit(1);
		}
	}
	
	function extractAndTransformData($m, $taskid){
		$job = new stdClass();
		$job->clips = array();
		$job->mixins = array();
		$xmlMessage = simplexml_load_string(html_entity_decode($m));
		foreach ($xmlMessage->composition->track as $track) {
			foreach ($track->event as $evt) {
				$id = (string)$evt->source->id;
				if(!in_array($id, $job->clips)){
					$job->clips[] = $id;
				}
				$mxin = new stdClass();
				$mxin->start = intval($evt['startTime']) / 44100;
				$mxin->duration = intval($evt['totalTime']) / 44100;
				$mxin->id = (string)$evt->source->id;
				$job->mixins[] = $mxin;
			}
		}
		$job->id = intval($xmlMessage->id);
		$job->mtime = intval($xmlMessage->mtime);
		$this->writeJson($job, $taskid);
	}
	
	function writeTaskTo($task){
	
		if(!$this->isLocked()){
			$this->_logger->log("ERROR - you can only store a task to the file system when the fileManager wrapper has a locked state. Please lock it, then start some process entering this method. This method cannot be entered when this instance is not locked.");
			exit(1);
		}
		
		$id = $task->getMessageId();
		$m = $task->getMessageText();
		$this->writeMessageFile($id, $m);
		$this->writePopReceiptFile($id, $task->getPopReceipt());
		
		// extract the data to a form that can be processed more easily
		$this->extractAndTransformData($m, $id);
		
		// now that the write has succeeded, we change the lock for the real file ...
		$this->_file = $id;
		unlink($this->_location . self::LOCKFILE_NAME);
	}
	
	function storeEcasoundCommand($ecasoundCommand){
		$this->_ecasound_command_file_loc = $this->_location . $this->_file . $this->_ecasound_command_postfix;
		$this->_logger->log("writing " . $this->_ecasound_command_file_loc);
		if(false === file_put_contents($this->_ecasound_command_file_loc, $ecasoundCommand)){
			$this->_logger->log("ERROR - could not write to " . $this->_ecasound_command_file_loc);
			exit(1);
		}
	}
	
	function writeJson($json, $taskid){
		$this->_input_message_json_file_loc = $this->_location . $taskid . $this->_jsontask_postfix;
		$this->_logger->log("writing " . $this->_input_message_json_file_loc);
		if(false === file_put_contents($this->_input_message_json_file_loc, json_encode($json))){
			$this->_logger->log("ERROR - could not write to " . $this->_input_message_json_file_loc);
			exit(1);
		}
	}
	
	function readJson(){
		$this->_input_message_json_file_loc = $this->_location . $this->_file . $this->_jsontask_postfix;
		$ms = file_get_contents($this->_input_message_json_file_loc);
		if(false === $ms){
			$this->_logger->log("ERROR - could not read " . $this->_input_message_json_file_loc);
		}
		return json_decode($ms);
	}
	
	function flush(){
		$this->_logger->log("flushing");
		$this->removeFiles($this->_location);
		$this->removeFiles($this->_output_location);
	}
	
	function removeFiles($d){
		$files = glob($d . '*'); // get all file names
		foreach($files as $file){ // iterate files
			if(is_file($file)){
				if(false === unlink($file)){ // delete file
					$this->_logger->log("ERROR - could not remove file " . $file);
				}
			}
		}
	}
	
	function messageName(){
		return $this->_file;
	}
	
	function mp3s(){
		$m = $this->readJson();
		return $m->clips;
	}
	
	function mixins(){
		$m = $this->readJson();
		return $m->mixins;
	}
	
	function id(){
		$m = $this->readJson();
		return $m->id;
	}
	
	function mtime(){
		$m = $this->readJson();
		return $m->mtime;
	}
}