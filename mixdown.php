<?php

//======================================================================
// CRON-TRIGGERED SCRIPT FOR MAKING A MIXDOWN OF A COMPOSITION
//======================================================================

//-----------------------------------------------------
// Introduction
//-----------------------------------------------------

/* How Can The Script Be Triggered */

// It can be triggered in many ways. In this case I did it with a cron job like this.
// * * * * * for i in {0..59}; do curl http://localhost/mixdown.php && sleep 1; done;

/* System Requirements */

// The script was run on OSX with lame, mpg123 and ecasound installed

/* What Does The Script Do */

// Summary: it makes a mixdown of multiple mp3 files 
// according to a stored composition XML structure 
// and uploads one resulting mp3 to a storage container.

// To detail all the steps of the script:

# It makes preparations:
## Initializes a Logger
## Loads Settings from a Settings File.
## Inspects which local files are present.
## If the system is already at work with some files it will exit.
## If the system is not busy it will place a lock to prevent parallel processing.
## Pick the first available mixdown task from a message queue.
## Write the xml composition from the task, the queue pop receipt and a json extract on disk.
## Remove the lock from disk (from then on the file manager says busy because of the files).
## Adds /usr/local/bin to the path (necessary to be able to use the brew-installed programs on OSX)
# It collects source media:
## if not yet present, downloads mp3 files from cloud storage container
## if not yet present, converts the mp3 file to wav format
# It makes the composite sound file:
## It compiles the composition from the task into an ecasound command.
## It stores the command to disk for debugging purposes
## It executes the ecasound command creating the resulting mix as wav
## It compresses the resulting wav to mp3 using the lame encoder
## It uploads the resulting mp3 to azure storage
## It posts a command message to another storage queue in order to update the playlist
## When everything was successful it deletes the task from the task queue

/*
* Initialize the logger with a prefix.
* The logger will use the standard system logging.
*/
require_once 'logger.php';
$l = new MyLogger();
$l->setPrefix('adtmx - ');
$l->log('hi');

// exit;
/**
* Load the Settings file and use a validator to see if the settings are good.
*/
$settings = include('settings.php');
$l->log('settings loaded');

/*
* Load a file manager. It makes sure that the file collection is in good shape and can be used to process one composition at a time.
* Because the mix down task can be multi threaded by nature it makes no sense from a performance perspective to allow multiple such processes.
* NOTE - When you would have shared storage between compute resources, this would be an entirely different concern.
*/
require_once 'filemanager.php';
$fileManager = new FileManager($settings->currently_processed_dir, $settings->temporary_output_drop_dir, $l);
$fileManager->seeWhichFilesArePresent();
$l->log("files inspected");

if($fileManager->looksBusy($settings->total_processing_time_allowed_per_task)){
	$l->log("system is busy, exit");
	exit(0);
}else{
	$fileManager->lock();
	$l->log("placed lock allright, let's see if there is a task");
}
/*
* The task picker will connect to an Azure Storage Queue to pick a mixdown task (if present).
*/
require_once 'taskpicker.php';
$taskPicker = new TaskPicker();
$task = $taskPicker->fetchTaskFrom($settings->storage_connection_string, $settings->storage_queue_name, $l);
if($task === false){
	$l->log("no task found, unlock and exit");
	$fileManager->unlock();
	exit(0);
}else{
	$l->log("task fetched");
}
/*
* Write the task to the filesystem, replacing the lock.
* At this stage the task has still the form of an XML format like it was stored by version 1 of the composition tool.
* To make mp3's, mix events, modified time and id more easily retrieved and processed, there is also an extract / transform of the XML.
*/
$fileManager->writeTaskTo($task);
/*
* Add /usr/local/bin to the path (necessary to be able to use the brew-installed programs on OSX)
* the concerned programs are
* - mpg123
* - lame
* - ecasound
*/
$path = getenv('PATH'); putenv( "PATH=" . $path . ":/usr/local/bin" );
/*
* collect the source files (if not already present)
* the same method also converts them to wav (if not already done)
*/
require_once 'sourcefilecollector.php';
$sourceFileCollector = new SourceFileCollector($l, $settings->local_source_files_dir, $settings->source_file_storage);
$sourceFileCollector->collect($fileManager->mp3s());
/**
* compile the mix events from the task into an ecasound command
*/
require_once 'ecasoundscriptcompiler.php';
$ecasoundScriptCompiler = new EcasoundScriptCompiler($settings->local_source_files_dir, $settings->temporary_output_drop_dir);
$l->log("composing command for [" . $fileManager->id() . "] with " . count($fileManager->mixins()) . " clips");
$cmd = $ecasoundScriptCompiler->compile($fileManager->mixins(), $fileManager->id(), $l);

$resultingFileUrl = "";

if($cmd){
	/*
	* store it for debugging purposes and execute it
	*/
	$fileManager->storeEcasoundCommand($cmd);
	$l->log(shell_exec($cmd));
	$l->log("resulting wav file should be at " . $ecasoundScriptCompiler->getOutputFileFull());
	/*
	* use the lame encoder to compress the resulting auio
	*/
	$lame = "lame -V2 " . $ecasoundScriptCompiler->getOutputFileFull() . " " . $ecasoundScriptCompiler->getOutputFileMp3() . " 2>&1";
	$l->log("converting to mp3: " . $lame);
	$l->log(shell_exec($lame));
	/*
	* upload the resulting mp3 file to the storage container
	*/
	require_once 'mp3storage.php';
	$upl = new Mp3Storage($l, $settings->target_file_storage_connection_string, $settings->target_file_storage_account_name, $settings->target_file_storage_container_name);
	$upl->write($ecasoundScriptCompiler->getOutputFileMp3(), $fileManager->id() . '.MP3');
	$resultingFileUrl = $upl->getUrl($fileManager->id() . '.MP3');
}

$l->log("result file at " . $resultingFileUrl);

$taskPicker->storeResultToPlaylist($fileManager->id(), $fileManager->mtime(), $resultingFileUrl, $settings->playlist_queue_name);
$taskPicker->deleteMessageFromQueue();

// remove all the intermediate stuff
$fileManager->flush();
$l->log("done, bye");

?>