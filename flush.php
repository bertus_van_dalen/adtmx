<?php


require_once 'vendor/autoload.php';
require_once 'logger.php';
require_once 'filemanager.php';


// say hello
$logger = new MyLogger();
$logger->log('flush');





// load the settings from the settings file
$settings = include('settings.php');

// remove the lock
$fileManager = new FileManager($settings->currently_processed_dir, $settings->temporary_output_drop_dir, $logger);
$logger->log('filemanager created');

$fileManager->flush();

echo "ok";
