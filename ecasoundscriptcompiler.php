<?php

class EcasoundScriptCompiler {
	
	private $_sourcesLocation; 
	private $_targetsLocation;
	private $_id;
	
	
	function __construct($sourcesLocation, $targetsLocation) {
		$this->_sourcesLocation = $sourcesLocation;
		$this->_targetsLocation = $targetsLocation;
    }
	
	function getOutputFileFull(){
		return $this->_targetsLocation . $this->_id . ".WAV";
	}
	
	function getOutputFileMp3(){
		return $this->_targetsLocation . $this->_id . ".MP3";
	}
	
	function compile($mixins, $id, $logger){
		
		$this->_id = $id;
		
		$first = floatval(3600);
		foreach($mixins as $mixin){
			if($first > $mixin->start){
				$first = floatval($mixin->start);
			}
		}
		
		if($first == 3600){
			return "";
		}
		
		$cmd = "ecasound -D -z:mixmode,sum ";
		$chainCount = 1;
		foreach($mixins as $mixin){
			$at = floatval($mixin->start) - $first;
			$cmd .= "-a:" . $chainCount . " -i playat," . $at . ",select,0," . floatval($mixin->duration) . "," . $this->_sourcesLocation . $mixin->id . ".WAV ";
			$chainCount++;
		}
		$cmd .= "-a:all -o " . $this->getOutputFileFull() . " 2>&1";
		return $cmd;
	} 
}