<?php

class MyLogger {
    
	private $_prefix = "";
	
	function setPrefix($prefix){
		$this->_prefix = $prefix;
	}
	
	function log($m) { 
        error_log($this->_prefix . $m);  
    }
}