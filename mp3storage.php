<?php

require_once 'vendor/autoload.php';
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Common\ServiceException;

class Mp3Storage {
	
	private $_logger;
	private $_connectionString;
	private $_storageAccountName;
	private $_containerName;
	
	
	function __construct($logger, $conn, $storage_account_name, $container_name){
		$this->_logger = $logger;
		$this->_connectionString = $conn;
		$this->_storageAccountName = $storage_account_name;
		$this->_containerName = $container_name;
	}
	
	function write($fileFullPath, $blob_name){
		
		$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($this->_connectionString);
		$content = fopen($fileFullPath, "r");
        
        try    {
            //Upload blob
            $blobRestProxy->createBlockBlob($this->_containerName, $blob_name, $content);
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            $this->_logger->log($code . ": " . $error_message . "<br />");
        }
	}
	
	function getUrl($blob_name){
		return "https://" . $this->_storageAccountName . ".blob.core.windows.net/" . $this->_containerName . "/" . $blob_name;
	}
}