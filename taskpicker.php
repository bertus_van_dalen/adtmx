<?php

require_once 'vendor/autoload.php';
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Common\ServiceException;
use MicrosoftAzure\Storage\Queue\Models\ListMessagesOptions;

class TaskPicker
{
	private $_prox;
	private $_queue;
	private $_input_message_id;
	private $_input_message_pop_receipt;
	private $_logger;
	
	function fetchTaskFrom($storageConnectionString, $queueName, $logger){
		
		// Create queue REST proxy.
		$this->_prox = ServicesBuilder::getInstance()->createQueueService($storageConnectionString);
		$this->_queue = $queueName;
		$this->_logger = $logger;
		
		try    
		{
			$listMessagesOptions = new ListMessagesOptions();
			$listMessagesOptions->setNumberOfMessages(1);
			$listMessagesResult = $this->_prox->listMessages($queueName, $listMessagesOptions);
		}
		catch(ServiceException $e){
			// Handle exception based on error codes and messages.
			// Error codes and messages are here:
			// http://msdn.microsoft.com/library/azure/dd179446.aspx
			$code = $e->getCode();
			$error_message = $e->getMessage();
			$logger->log("ERROR " . $code . "-" . $error_message);
		}
		
		$messages_input = $listMessagesResult->getQueueMessages();  
		$messages_input_count = count($messages_input);
		
		if($messages_input_count <= 0){
			return false;
		}
		if($messages_input_count > 1){
			$logger->log("ERROR - inexpected behavior fetching more than one message? Maybe the library was changed?");
		}
		
		$t = $messages_input[0];
		
		
		
		$this->_input_message_id = $t->getMessageId();
		$this->_input_message_pop_receipt = $t->getPopReceipt();
		
		$dequeueCount = $t->getDequeueCount();
		
		$logger->log("dequeue count " . $dequeueCount);
		
		if($dequeueCount > 5){
			$this->deleteMessageFromQueue();
			return false;
		}
		
		return $t;
	} 
	
	function deleteMessageFromQueue(){
		// use callback client for the callback
        try{
        	$this->_logger->log("deleting from queue the message " . $this->_input_message_id);
			$this->_prox->deleteMessage($this->_queue, $this->_input_message_id, $this->_input_message_pop_receipt);
        }
        catch(ServiceException $e){
        	$code = $e->getCode();
        	$error_message = $e->getMessage();
        	$this->_logger->log("ERROR while trying to delete the message after successful processing: " . $error_message);
        }
	}
	
	function storeResultToPlaylist($composition_id, $composition_mtime, $resulting_file_url, $queue_name){
		try    {
			// Create message.\
			$msg = array('id' => $composition_id, 'mtime' => $composition_mtime, 'url' => $resulting_file_url);
			$msg = json_encode($msg);
			$msg = base64_encode($msg);
			$this->_prox->createMessage($queue_name, $msg);
		}
		catch(ServiceException $e){
			// Handle exception based on error codes and messages.
			// Error codes and messages are here:
			// http://msdn.microsoft.com/library/azure/dd179446.aspx
			$code = $e->getCode();
			$error_message = $e->getMessage();
			$this->_logger->log("ERROR while trying to send the command for resulting playlist processing: " . $error_message);
		}
	}
}





